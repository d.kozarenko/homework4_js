"use strict";
function createNewUser() {
    let userFirstName = prompt("Enter your name please: ");
    let userLastName = prompt("Enter your last name please: ");
    let newUser = {
        _firstName: userFirstName,
        _lastName: userLastName,
    }
    return newUser;
};
const newUser = createNewUser();
Object.defineProperty(newUser, "firstName", {
    setFirstName: function (value) {
        this._firstName = value;
    },
    get: function () {
        return this._firstName;
    },
});
Object.defineProperty(newUser, "lastName", {
    setLastName: function (value) {
        this._lastName = value;
    },
    get: function () {
        return this._lastName;
    },
});
newUser.getLogin = function () {
    return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
};
console.log(newUser.getLogin());
